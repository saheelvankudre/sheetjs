#!/bin/bash

# Print the current directory
echo "Current directory: $(pwd)"

# Run Basic Access Control Policy
./tools/opa_linux_amd64 eval -i ./tools/input.json -d ./tools/example.rego "data.main.allow"

# Run Data Validation Policy
./tools/opa_linux_amd64 eval -i ./tools/input.json -d ./tools/example.rego "data.main.valdation[msg]"

# Run Password Strength Policy for Users
./tools/opa_linux_amd64 eval -i ./tools/input.json -d ./tools/example.rego "data.main.pswrd[msg] { input.user.role == \"user\" }"

# Run Compliance Policy
./tools/opa_linux_amd64 eval -i ./tools/input.json -d ./tools/example.rego "data.main.compliance[msg]"
