from flask import Flask, request

app = Flask(__name__)

@app.route('/opa-webhook', methods=['POST'])
def opa_webhook():
    data = request.json
    # Process OPA evaluation results here
    print(data)
    return 'OK'

if __name__ == '__main__':
    # Ensure that the OPA server is accessible externally
    app.run(host='0.0.0.0', port=8181)
