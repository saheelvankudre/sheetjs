package main

# Basic Access Control Policy
allow {
    input.user.role == "admin"
}

# Data Validation Policy
validation[msg] {
    input.request.path == ["users"]
    not is_object(input.request.body)
    msg = "Invalid data format for user creation."
}

# Password Strength Policy for Admins
pswrd[msg] {
    input.user.role == "admin"
    input.user.password != "password"
    msg = "Weak password detected for admin. Please choose a stronger password."
}

# Compliance Policy
compliance[msg] {
    input.resource.type == "database"
    input.resource.encryption == "disabled"
    msg = "Database encryption is disabled, which is not compliant."
}
